# Fake_jump
* This is my first game with the Phaser framework. Phaser is a framework made with Javascript and concepts about canvas.
* Current version Phaser: 3
* Language used to make this game: Javascript (ECMAScript 2018)

* How you can install:
<br/>-STEP 1: Install the Node JS server (free): https://www.npmjs.com/package/http-server
<br/>-STEP 2: After extract the files, in a folder before the folder fake_jump, run the command line **$ http-server "fake_jump-master"**
<br/>-STEP 3: Open the web page in address http://127.0.0.1:8080. The address can be generated different, this depend of your computer.
<br/>-STEP 4: When the page is open, press ctrl + F5 one time for enable the sound.

* How you can play:
<br/>-The objective of this game is collect the max of stars. The game starts with two players. For control players you can use the keys W, A, D or the arrows left, right and up.