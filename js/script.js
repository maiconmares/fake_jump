/*Estamos usando funções anônimas para, além de proteger as variáveis contra mal uso, não termos problemas com 
variáveis globais*/
(function(){
	//param2. width, param3.height, param1. tecnologia de renderização(Se passarmos Phaser.AUTO ele escolhe automaticamente)
	/*param4. objeto que representa o estado ou a fase do jogo se tivermos diversas fases. Caso isso ocorra, 
	eu devo mudar os estados de acordo com o curso do jogo
	*/
	var configurations = {
    	type: Phaser.AUTO,
    	width: 800,
    	height: 600,
    	physics: {
	        default: 'arcade',
	        arcade: {
	            gravity: { y: 500 },
	            debug: false
	        }
    	},
    	scene: {
	        preload: preloadF,
	        create: createF,
	        update: updateF
    	}
	};
	//Instância do objeto game do Phaser
	var game = new Phaser.Game(configurations);

	var platforms;
	var keyW;
	var keyA;
	var keyD;
	var scoreP1 = 0;
	var scoreP2 = 0;
	var scoreTextP1;
	var scoreTextP2;
	var textWin;
	var namePlayer1;
	var namePlayer2;

	function getNames() {
		namePlayer1 = prompt("Digite o nome do jogador 1:");
		namePlayer2 = prompt("Digite o nome do jogador 2:");
	}
	//As funções abaixo são os valores passados para o objeto (param4. da instância de Phaser)
	/*A função preloadF serve para eu carregar os recursos necessários (imagens, sons, etc) durante a 
	execução do jogo*/
	function preloadF(){
		this.load.image('sky', 'img/sky.png');
    	this.load.image('ground', 'img/platform.png');
    	this.load.image('star', 'img/star.png');
    	this.load.image('bomb', 'img/bomb.png');
    	this.load.spritesheet('dude', 
        'img/dude.png',
        { frameWidth: 32, frameHeight: 48 });
	}
	/*A função createF é onde eu crio variáveis, arrays e outras coisas que precisam ser criadas durante o
	jogo*/
	function createF(){
		getNames();
		this.add.image(400, 300, 'sky');

		//Adiciona elementos de mesmo tipo a um grupo
		/*physics é um sistema que vem com auxílio à configuração de gravidade. É um grupo de física
		para corpos estáticos*/
		platforms = this.physics.add.staticGroup();

		//Criação efetiva de um elemento do grupo platform

		platforms.create(-100, 250, 'ground').setScale(2).refreshBody();
		platforms.create(900, 390, 'ground').setScale(2).refreshBody();
		platforms.create(400, 600, 'ground').setScale(2).refreshBody();
		platforms.create(400, 100, 'ground');

		//Adiciona o jogador ao jogo
		player = this.physics.add.sprite(100, 544, 'dude');
		player2 = this.physics.add.sprite(200, 544, 'dude');

		//Define a elasticidade do personagem, ou seja, ao tocar o chão após um pulo, ele pula novamente.
		player.setBounce(0.2);
		player.setCollideWorldBounds(true);

		player2.setBounce(0.2);
		player2.setCollideWorldBounds(true);

		//Animação para movimento indo para esquerda
		this.anims.create({
		    key: 'left',
		    frames: this.anims.generateFrameNumbers('dude', { start: 0, end: 3 }),
		    frameRate: 10,
		    repeat: -1
		});

		//Animação para movimento indo para a cima
		this.anims.create({
		    key: 'turn',
		    frames: [ { key: 'dude', frame: 4 } ],
		    frameRate: 20
		});

		//Animação para movimento indo para direita
		this.anims.create({
		    key: 'right',
		    frames: this.anims.generateFrameNumbers('dude', { start: 5, end: 8 }),
		    frameRate: 10,
		    repeat: -1
		});

		/*O objeto collider monitora dois objetos e verifica e corrige sobreposição e colisão entre eles.
		Ele atua sobre todos os elementos do grupo platforms. Esse comportamento é o mesmo para qualquer grupo*/
		this.physics.add.collider(player, platforms);

		this.physics.add.collider(player2, platforms);

		this.physics.add.collider(player, player2);

		//Adição do grupo stars que utiliza física dinâmica
		stars = this.physics.add.group({
	    	key: 'star', //imagem
	    	repeat: 11,	//criação de 11 filhos iguais
	    	setXY: { x: 12, y: 0, stepX: 70 } //posicionamento com passo de 70px
		});

		//Pulo aleatório (elasticidade)
		stars.children.iterate(function (child) {

    	child.setBounceY(Phaser.Math.FloatBetween(0.4, 0.8));
		});

		this.physics.add.collider(stars, platforms);

		//Verifica a sobreposição do personagem e da estrela
		this.physics.add.overlap(player, stars, collectStar, null, this);
		this.physics.add.overlap(player2, stars, collectStar, null, this);

		//Quando o jogador toca a estrela sua física é desabilitada (param1.) e depois seu corpo (param2.)
		function collectStar (player, star)
		{
    		star.disableBody(true, true);
    		
    		//Muda o texto de score na tela de acordo com a coleta de estrelas
    		if(player == player2){
	    		scoreP2 += 10;
	    		scoreTextP2.setText(namePlayer2 + ': ' + scoreP2);
			} else {
				scoreP1 += 10;
				scoreTextP1.setText(namePlayer1 + ': ' + scoreP1);
			}

			//Se todas as estrelas forem coletadas uma bomba é solta na direção oposta ao player 1
			if (stars.countActive(true) === 0)
		    {
		        stars.children.iterate(function (child) {

		            child.enableBody(true, child.x, 0, true, true);

		        });

		        //Joga duas bombas. Cada bomba é lançada na posição contrária ao player (implementar uma Matemática mais justa futuramente)
		        var xPlayer1 = (player.x < 400) ? Phaser.Math.Between(400, 800) : Phaser.Math.Between(0, 400);
		        var xPlayer2 = (player2.x < 400) ? Phaser.Math.Between(400, 800) : Phaser.Math.Between(0, 400);

		        var bomb1 = bombs.create(xPlayer1, 16, 'bomb');
		        var bomb2 = bombs.create(xPlayer2, 16, 'bomb');

		        bomb1.setBounce(1);
		        bomb1.setCollideWorldBounds(true);
		        bomb1.setVelocity(Phaser.Math.Between(-200, 200), 20);

		        bomb2.setBounce(1);
		        bomb2.setCollideWorldBounds(true);
		        bomb2.setVelocity(Phaser.Math.Between(-200, 200), 20);

		    }
		}

		//Mostra o texto de score na tela
		scoreTextP1 = this.add.text(16, 16, namePlayer1 + ': 0', { fontSize: '32px', fill: '#000' });
		scoreTextP2 = this.add.text(560, 16, namePlayer2 + ': 0', { fontSize: '32px', fill: '#000' });
		textWin = this.add.text(250, 180, '', {fontSize: '32px', fill: '#000'});

		//Grupo bombas
		bombs = this.physics.add.group();

		this.physics.add.collider(bombs, platforms);

		//Se a colisão ocorrer entre o player e a bomba a função hitBomb é chamada
		this.physics.add.collider(player, bombs, hitBomb, null, this);
		this.physics.add.collider(player2, bombs, hitBomb, null, this);

		function victory(){
			if (scoreP1 == scoreP2){
				textWin.setText('Ocorreu um empate!');
			}
			else if (scoreP1 > scoreP2) {
		    	textWin.setText('O ' + namePlayer1 + ' venceu!');
		    } else {
		    	textWin.setText('O ' + namePlayer2 + ' venceu!');
		    }
		}

		//Se um dos players for acertado ele é pintado de vermelho e o jogo acaba
		function hitBomb (player, bomb)
		{

			victory();

		    this.physics.pause();

		    player.setTint(0xff0000);

		    player.anims.play('turn');

		    gameOver = true;
		}
	}
	//Na função updateF eu coloco toda a lógica que deve ser verificada durante a execução do jogo
	function updateF(){
		/*Este método preenche todas as teclas do computador. Eu posso escolher a tecla que eu quiser e 
		a animação quando essa for pressionada*/
		cursors = this.input.keyboard.createCursorKeys();

		//Adiciona teclas diferentes para o player2
		keyW = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W);
		keyA = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
		keyD = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);


		//Configuração de cada tecla
		if (cursors.left.isDown)
		{
		    player.setVelocityX(-240);

		    player.anims.play('left', true);
		}
		else if (cursors.right.isDown)
		{
		    player.setVelocityX(240);

		    player.anims.play('right', true);
		}
		else
		{
		    player.setVelocityX(0);

		    player.anims.play('turn');
		}

		//Permite pular, mas verifica se o jogador está pressionando a tecla para cima e tocando o chão
		if (cursors.up.isDown && player.body.touching.down)
		{
		    player.setVelocityY(-500);
		}

		//Configuração das teclas para o player2
		if (keyA.isDown)
		{
		    player2.setVelocityX(-240);

		    player2.anims.play('left', true);
		}
		else if (keyD.isDown)
		{
		    player2.setVelocityX(240);

		    player2.anims.play('right', true);
		} else {
			player2.setVelocityX(0);

		    player2.anims.play('turn');
		}

		if (keyW.isDown && player2.body.touching.down)
		{
		    player2.setVelocityY(-500);
		}

	}
}());